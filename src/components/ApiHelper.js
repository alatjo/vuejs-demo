import VueAxios from 'vue-axios';
import axios from 'axios';
import Vue from 'vue';
import Config from '../resources/config.json';

Vue.use(VueAxios, axios);

export default class Api {
  static apiCall() {
    return Vue.axios.get(Config.backend.apiUrl)
    .then(response => response.data)
    .catch(error => console.log(error)); //eslint-disable-line
  }
}
