import { Line } from 'vue-chartjs';

export default Line.extend({
  props: ['chartData', 'chartLabels'],
  data() {
    return {
      dataset: this.chartData,
      labels: this.chartLabels,
    };
  },
  mounted() {
    this.gradient = this.$refs.canvas.getContext('2d').createLinearGradient(0, 0, 0, 450);
    this.dataset[0].backgroundColor = this.gradient;
    this.dataset[1].backgroundColor = this.gradient;
    this.dataset[2].backgroundColor = this.gradient;

    this.renderChart({
      labels: this.labels,
      datasets: this.dataset,
    },
      {
        responsive: true,
        maintainAspectRatio: false,
        elements: {
          line: {
            tension: 0,
          },
        },
        scales: {
          xAxes: [
            {
              type: 'time',
              unit: 'month',
              time: {
                displayFormats: {
                  week: 'MMM DD',
                },
                tooltipFormat: 'MMM DD',
              },
              scaleLabel: {
                display: true,
                labelString: 'Date',
              },
            },
          ],
          yAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: 'value',
              },
            },
          ],
        },
      });
  },
});
