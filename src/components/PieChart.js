import { Pie } from 'vue-chartjs';

export default Pie.extend({
  props: ['chartData', 'chartLabels'],
  data() {
    return {
      dataset: [],
      labels: [],
    };
  },
  watch: {
    chartData() {
      const localLabels = JSON.parse(JSON.stringify(this.chartLabels));
      const localData = JSON.parse(JSON.stringify(this.chartData));
      this.render(localLabels, localData);
    },
  },
  methods: {
    render(localLabels, localData) {
      this.renderChart({
        labels: localLabels,
        datasets: localData,
      }, { responsive: true, maintainAspectRatio: false });
    },
  },
});
