// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import Navigation from './components/Navigation';
import MyFooter from './components/MyFooter';
import router from './router';

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#navigation',
  template: '<Navigation/>',
  components: { Navigation },
});

new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App },
});

new Vue({
  el: '#myFooter',
  template: '<MyFooter/>',
  components: { MyFooter },
});
