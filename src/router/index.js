import Vue from 'vue';
import Router from 'vue-router';
import Header from '@/components/Header';
import LineComponent from '@/components/LineComponent';
import LineGoogleChartComponent from '@/components/LineGoogleChartComponent';
import PieComponent from '@/components/PieComponent';
import Data from '@/components/Data';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Header',
      component: Header,
    },
    {
      path: '/Line',
      name: 'LineChart',
      component: LineComponent,
    },
    {
      path: '/LineGoogleChart',
      name: 'lineGoogleChartComponent',
      component: LineGoogleChartComponent,
    },
    {
      path: '/Pie',
      name: 'PieChart',
      component: PieComponent,
    },
    {
      path: '/Data',
      name: 'Data',
      component: Data,
    },
  ],
});
